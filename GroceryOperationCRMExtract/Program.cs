﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Drawing;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Windows.Forms;
using System.Configuration;
//using System.Web.Services.Protocols;
using GroceryOperationCRMExtract.GRCRightNowReference;
using GemBox.Spreadsheet;

////////////////////////////////////////////////////////////////////////////////
//Copyright © 2006-2012, Oracle and/or its affiliates. All rights reserved.
//Sample code provided for training purposes only. This sample code is
//provided "as is" with no warranties of any kind express or implied.
//
//File: BasicQueryObjects.cs
//
//Comments: Basic sample that demonstrates the QueryObjects operation
//
//Notes: 
//
//
//Pre-Conditions: 
//1. You must have II_Connect_Enabled for your RightNow CX May 10 or newer site
//2. You must have a staff account whose profile has the Public SOAP bit enabled
//
////////////////////////////////////////////////////////////////////////////////
namespace GroceryCRMData
{
    class GroceryCRMDataExtract
    {
        static RightNowSyncPortClient _client;

        static GroceryCRMDataExtract()
        {
            _client = new RightNowSyncPortClient();
            _client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["RNUSER"].ToString();
            _client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["RNPass"].ToString();
        }

        static DataTable buildTable(string tablename, string filter = "")
        {
            string query = "SELECT * FROM " + tablename;
            if (filter != "")
            {
                query = query + " WHERE " + filter;
            }

            ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
            clientInfoHeader.AppID = "Basic Objects Sample";

            APIAccessRequestHeader apiRequestHeader = new APIAccessRequestHeader();
            APIAccessResponseHeaderType respHeaderType = new APIAccessResponseHeaderType();
            DataTable dt = new DataTable();
            ArrayList strData = new ArrayList();


            byte[] rawResults = null;
            string delimiter = "~";
            try
            {
                CSVTableSet queryCSV = new CSVTableSet();
                respHeaderType = _client.QueryCSV(clientInfoHeader, apiRequestHeader, query, 10000, delimiter, false, true, out queryCSV, out rawResults);

                CSVTable[] csvTables = queryCSV.CSVTables;
                DataRow dr = null;
                foreach (CSVTable tbl in csvTables)
                {
                    string colHeadings = tbl.Columns;
                    //string tmpTable = "";
                    string[] rowdata = tbl.Rows;

                    string[] colnames = tbl.Columns.ToString().Split(delimiter.ToCharArray());


                    strData.Add(tbl.Columns);
                    foreach (string col in colnames)
                    {
                        dt.Columns.Add(col);
                    }

                    //int counter = 0;
                    foreach (string data in rowdata)
                    {
                        int fldnum = 0;
                        string[] fld = data.Split(delimiter.ToCharArray());
                        dr = dt.NewRow();
                        foreach (string d in fld)
                        {
                            dr[fldnum] = d;
                            fldnum++;
                        }
                        dt.Rows.Add(dr);
                        strData.Add(data);
                    }


                    if (ConfigurationManager.AppSettings["DEBUG"].ToString() == "YES")
                    {
                        Console.WriteLine("table " + tablename + " created with " + dt.Rows.Count);
                        FileStream fileout = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + tablename + ".txt", FileMode.OpenOrCreate);

                        StreamWriter fw = new StreamWriter(fileout);
                        foreach (string dataline in strData)
                        {
                            fw.WriteLine(dataline);
                        }
                        fw.Close();
                    }


                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("Sumting wong...." + ex.Message.ToString());
            }

            return dt;
        }

        static string LookupValues(string columnname, string value)
        {
            string newValue = value;
            if (value != "")
            {
                switch (columnname)
                {
                    case "Status":
                        if (value == "1") newValue = "Active";
                        if (value == "2") newValue = "InActive";
                        break;
                    case "Franchise":
                        if (value == "0") newValue = "No";
                        if (value == "1") newValue = "Yes";
                        break;
                    case "License":

                        switch (value)
                        {
                            case "1":
                                newValue = "BEER & CIDER";
                                break;
                            case "2":
                                newValue = "RESTRICTED";
                                break;
                            case "3":
                                newValue = "UNRESTRICTED";
                                break;
                            case "4":
                                newValue = "WINE BOUTIQUE";
                                break;
                        }
                        break;
                    case "Category":
                        switch (value)
                        {
                            case "1":
                                newValue = "A";
                                break;
                            case "2":
                                newValue = "B";
                                break;
                        }
                        break;

                    case "Region":
                        switch (value)
                        {
                            case "1":
                                newValue = "EAST";
                                break;
                            case "2":
                                newValue = "GTA";
                                break;
                            case "3":
                                newValue = "NORTH";
                                break;
                            case "4":
                                newValue = "WEST";
                                break;
                        }
                        break;

                    case "Warehouse":
                        switch (value)
                        {
                            case "1":
                                newValue = "DUR";
                                break;
                            case "2":
                                newValue = "DUR-X-O";
                                break;
                            case "3":
                                newValue = "DUR-X-T";
                                break;
                            case "4":
                                newValue = "LON";
                                break;
                        }
                        break;
                    case "Order_Method":
                        switch (value)
                        {
                            case "1":
                                newValue = "EDI";
                                break;
                            case "2":
                                newValue = "Manual";
                                break;

                        }
                        break;
                    case "Invoice_Method":
                        switch (value)
                        {
                            case "1":
                                newValue = "EDI";
                                break;
                            case "2":
                                newValue = "Manual";
                                break;

                        }
                        break;
                    case "Location_Type":
                        switch (value)
                        {
                            case "1":
                                newValue = "STORE";
                                break;
                            case "2":
                                newValue = "DC";
                                break;

                        }
                        break;
                    case "Carrier":
                        switch (value)
                        {
                            case "1":
                                newValue = "CAN. CART";
                                break;
                            case "2":
                                newValue = "MANITOULIN";
                                break;
                            case "3":
                                newValue = "MEL HALL";
                                break;
                            case "4":
                                newValue = "ROSEDALE";
                                break;
                            case "5":
                                newValue = "TBS";
                                break;
                        }
                        break;
                    default:
                        newValue = value;
                        break;
                }
            }
            return newValue;

        }

        static void Main()
        {

            try
            {

                DataTable dtStoreInfo = buildTable("Grocery.StoreInfo");
                DataTable dtStoreInfoNotes = buildTable("Grocery.StoreInfoNotes");
                DataTable dtTranche = buildTable("Grocery.Tranche", "StoreInfo > 0");
                DataTable dtReport = new DataTable();

                SortedList lstTranche = new SortedList();
                //Get the number of Tranches there is to build customer table.


                //Build new Report Table
                //dtReport = dtStoreInfo.Copy();
                //dtReport.Columns.Add("LCBO_Assigned_Store", typeof(UInt64));
                dtReport.Load(dtStoreInfo.CreateDataReader(), System.Data.LoadOption.OverwriteChanges);


                dtReport.Columns["LCBO_Assigned_Store"].SetOrdinal(0);
                dtReport.Columns["Status"].SetOrdinal(1);
                dtReport.Columns["Franchise"].SetOrdinal(2);
                dtReport.Columns["Store_Name"].SetOrdinal(3);
                dtReport.Columns["Company_Name"].SetOrdinal(4);
                dtReport.Columns["Address"].SetOrdinal(5);
                dtReport.Columns["City"].SetOrdinal(6);
                dtReport.Columns["Postal_Code"].SetOrdinal(7);
                dtReport.Columns["Store_Contact"].SetOrdinal(8);
                dtReport.Columns["Contact_Email"].SetOrdinal(9);
                dtReport.Columns["Telephone"].SetOrdinal(10);
                dtReport.Columns["BOL_Contact_1"].SetOrdinal(11);
                dtReport.Columns["BOL_Contact_2"].SetOrdinal(12);
                dtReport.Columns["BOL_Contact_3"].SetOrdinal(13);
                dtReport.Columns["Cost_Centre"].SetOrdinal(14);
                dtReport.Columns["Customer_Number_Oracle"].SetOrdinal(15);
                dtReport.Columns["Customer_Name_Oracle"].SetOrdinal(16);
                dtReport.Columns["Current_License_Type"].SetOrdinal(17);
                dtReport.Columns["AGCO_Authorization"].SetOrdinal(18);
                dtReport.Columns["Deactivation_Date"].SetOrdinal(19);
                dtReport.Columns["Deactivation_Reason"].SetOrdinal(20);
                dtReport.Columns["CAT_A_B"].SetOrdinal(21);
                dtReport.Columns["Region"].SetOrdinal(22);
                dtReport.Columns["Partner_Store"].SetOrdinal(23);
                dtReport.Columns["Partner_Store_Description"].SetOrdinal(24);
                dtReport.Columns["Warehouse"].SetOrdinal(25);
                dtReport.Columns["Order_Method"].SetOrdinal(26);
                dtReport.Columns["Invoice_Method"].SetOrdinal(27);
                dtReport.Columns["EDI_Number"].SetOrdinal(28);
                dtReport.Columns["Location_Type"].SetOrdinal(29);
                dtReport.Columns["Order_Day_by_9"].SetOrdinal(30);
                dtReport.Columns["Delivery_Day"].SetOrdinal(31);
                dtReport.Columns["Carrier"].SetOrdinal(32);
                dtReport.Columns["WB_first_order"].SetOrdinal(33);
                dtReport.Columns["WB_first_selling"].SetOrdinal(34);
                dtReport.Columns["WB_AGCO"].SetOrdinal(35);
                dtReport.Columns["WB_payment_terms"].SetOrdinal(36);
                dtReport.Columns["WB_license"].SetOrdinal(37);
                dtReport.Columns["Boutique_Region"].SetOrdinal(38);
                dtReport.Columns.Remove("CreatedByAccount");
                dtReport.Columns.Remove("CreatedTime");
                //dtReport.Columns.Remove("ID");
                dtReport.Columns.Remove("LookupName");
                dtReport.Columns.Remove("Organization");
                dtReport.Columns.Remove("UpdatedByAccount");
                dtReport.Columns.Remove("UpdatedTime");
                dtReport.Columns.Remove("WB_Region");


                //Remove fields from Tranche that is not needed
                dtTranche.Columns.Remove("LookupName");
                dtTranche.Columns.Remove("CreatedByAccount");
                dtTranche.Columns.Remove("CreatedTime");
                dtTranche.Columns.Remove("ID");
                dtTranche.Columns.Remove("UpdatedByAccount");


                //remove store 99999 from store
                foreach (DataRow drTestStores in dtReport.Select("LCBO_Assigned_Store = 99999"))
                {
                    dtReport.Rows.Remove(drTestStores);
                }

                //Tranche table clean up - remove stores in Tranche not in StoreInfo
                for (int i = 0; i < dtTranche.Rows.Count; i++)
                {
                    DataRow drTranche = dtTranche.Rows[i];
                    if (dtStoreInfo.Select("ID = '" + drTranche["StoreInfo"] + "'").Count() == 0)
                    {
                        dtTranche.Rows.Remove(drTranche);
                    }
                }

                var distinctNames = (from row in dtTranche.AsEnumerable()
                                     orderby row.Field<string>("Tranche_Num")
                                     select row.Field<string>("Tranche_Num")).Distinct();

                foreach (var TranchNums in distinctNames) { lstTranche.Add(Convert.ToInt16(TranchNums), TranchNums); };

                foreach (Int16 val in lstTranche.Keys)
                {
                    // dtReport.Columns.Add("Tranch_Num_" + val);
                    dtReport.Columns.Add("Tranche #" + val + " Authorization #");
                    dtReport.Columns.Add("Tranche #" + val + " Category");
                    dtReport.Columns.Add("Tranche #" + val + " License");
                    dtReport.Columns.Add("Tranche #" + val + " Order Date");
                    dtReport.Columns.Add("Tranche #" + val + " Selling Date");
                    dtReport.Columns.Add("Tranche #" + val + " Payment Terms");
                    dtReport.Columns.Add("Tranche #" + val + " Region");

                }
                dtReport.Columns.Add("Notes");




                foreach (DataRow drReport in dtReport.Rows)
                {
                    //Convert Lookup values
                    drReport["Status"] = LookupValues("Status", drReport["Status"].ToString());
                    drReport["Franchise"] = LookupValues("Franchise", drReport["Franchise"].ToString());
                    drReport["Current_License_Type"] = LookupValues("License", drReport["Current_License_Type"].ToString());
                    drReport["Cat_A_B"] = LookupValues("Category", drReport["Cat_A_B"].ToString());
                    drReport["Region"] = LookupValues("Region", drReport["Region"].ToString());
                    drReport["Warehouse"] = LookupValues("Warehouse", drReport["Warehouse"].ToString());
                    drReport["Order_Method"] = LookupValues("Order_Method", drReport["Order_Method"].ToString());
                    drReport["Invoice_Method"] = LookupValues("Invoice_Method", drReport["Invoice_Method"].ToString());
                    drReport["Location_Type"] = LookupValues("Location_Type", drReport["Location_Type"].ToString());
                    drReport["Carrier"] = LookupValues("Carrier", drReport["Carrier"].ToString());
                    drReport["WB_License"] = LookupValues("License", drReport["WB_License"].ToString());
                    drReport["Boutique_Region"] = LookupValues("Region", drReport["Boutique_Region"].ToString());





                    //Find Tranche info for the current Store
                    DataRow[] drTranches = dtTranche.Select("StoreInfo = '" + drReport["ID"].ToString() + "'");
                    foreach (DataRow drTranche in drTranches)
                    {
                        drReport["Tranche #" + drTranche["Tranche_Num"] + " Authorization #"] = drTranche["Authorization_Num"];
                        drReport["Tranche #" + drTranche["Tranche_Num"] + " Category"] = LookupValues("Category", drTranche["Category"].ToString());
                        drReport["Tranche #" + drTranche["Tranche_Num"] + " License"] = LookupValues("License", drTranche["License"].ToString());
                        drReport["Tranche #" + drTranche["Tranche_Num"] + " Order Date"] = drTranche["Order_Date"];
                        drReport["Tranche #" + drTranche["Tranche_Num"] + " Selling Date"] = drTranche["Selling_Date"];
                        drReport["Tranche #" + drTranche["Tranche_Num"] + " Payment Terms"] = drTranche["Payment_Terms"];
                        drReport["Tranche #" + drTranche["Tranche_Num"] + " Region"] = LookupValues("Region", drTranche["Region"].ToString());
                    }
                    StringBuilder strNotes = new StringBuilder();
                    foreach (DataRow drNotes in dtStoreInfoNotes.Select("StoreInfo = '" + drReport["ID"].ToString() + "'", "UpdatedTime DESC"))
                    {
                        //strNotes.Append(drNotes["UpdatedTime"] + " " + drNotes["StoreNotes"] + "\n");
                        strNotes.Append(DateTime.Parse(drNotes["UpdatedTime"].ToString()).ToString("yyyy-MM-dd HH:mm:ss") + " - " + drNotes["StoreNotes"].ToString() + "\n");
                    }
                    drReport["Notes"] = strNotes.ToString();


                }
                dtReport.Columns.Remove("ID");

                //Rename column name for report
                dtReport.Columns["LCBO_Assigned_Store"].ColumnName = "LCBO Assigned Store #";
                dtReport.Columns["AGCO_Authorization"].ColumnName = "AGCO Authorization #";
                dtReport.Columns["Partner_Store"].ColumnName = "Partner Store #";
                dtReport.Columns["WB_AGCO"].ColumnName = "WBCBB AGCO #";
                dtReport.Columns["WB_First_Order"].ColumnName = "WBCBB First Order";
                dtReport.Columns["WB_First_Selling"].ColumnName = "WBCBB First Selling";
                dtReport.Columns["WB_Payment_Terms"].ColumnName = "WBCBB Payment Terms";
                dtReport.Columns["WB_License"].ColumnName = "WBCBB License";
                dtReport.Columns["Boutique_Region"].ColumnName = "WBCBB Region";


                //Remove the _ character
                foreach (DataColumn col in dtReport.Columns)
                {
                    string colname = col.ColumnName.ToString().Replace("_", " ");
                    col.ColumnName = colname;

                }

                //Write to excel file
                SpreadsheetInfo.SetLicense(ConfigurationManager.AppSettings["GEMBOX"].ToString());

                var workbook = new ExcelFile();
                var worksheet = workbook.Worksheets.Add("Store Full Data");


                var options = new InsertDataTableOptions()
                {
                    ColumnHeaders = true,
                    StartRow = 0
                };

                //Convert text to number 
                options.DataTableCellToExcelCellConverting += (s, e) =>
                {
                    string textnumber = e.DataTableValue as string;
                    UInt32 number;
                    if (textnumber != null && UInt32.TryParse(textnumber, out number))
                        e.ExcelCellValue = number;
                };

                worksheet.InsertDataTable(dtReport, options);

                //Format Excel Column
                //Sort by LCBO Assigned Store#
                SortDescription excelsort = new SortDescription(0, true);
                worksheet.Cells.GetSubrangeAbsolute(1, 0, dtReport.Rows.Count + 1, dtReport.Columns.Count).Sort(excelsort);

                //Format to Header Row
                ExcelRow headerRow = worksheet.Rows[0];
                headerRow.Height = headerRow.Height * 4;
                headerRow.Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
                headerRow.Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                headerRow.Style.WrapText = true;
                headerRow.Style.FillPattern.SetSolid(Color.Blue);
                headerRow.Style.Font.Color = Color.White;
                headerRow.Style.Font.Weight = ExcelFont.BoldWeight;

                //Make all columns to AutoFit except for notes
                foreach (ExcelColumn excol in worksheet.Columns)
                {
                    excol.Width = excol.Width * 2;
                    //excol.AutoFit();

                }
                //Set NOTES to be 5 times width
                ExcelColumn NotesCell = worksheet.Columns[dtReport.Columns.Count - 1];
                NotesCell.Style.WrapText = false;
                // NotesCell.AutoFit();
                NotesCell.Width = NotesCell.Width * 5;

                for (int i = 1; i < worksheet.Rows.Count - 1; i++)
                {
                    ExcelRow exrow = worksheet.Rows[i];
                    exrow.Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                }

                //check is FreezePane has a value
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["FreezePaneYX"].ToString()) == false)
                {
                    string[] yx = ConfigurationManager.AppSettings["FreezePaneYX"].ToString().Split(',');

                    worksheet.Panes = new WorksheetPanes(PanesState.Frozen, Int32.Parse(yx[0]), Int32.Parse(yx[1]),"F2", PanePosition.BottomRight);
                }

                //get user's desktop path
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                string reportname = ConfigurationManager.AppSettings["ReportName"].ToString() + "." + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                workbook.Save(path + "\\" + reportname);
                MessageBox.Show("All Done! Report is saved onto your desktop. \nReport Name:" + reportname);

            }
            catch (Exception ex)
            {
                //Console.WriteLine("sumting wong..." + ex.Message.ToString());
                MessageBox.Show("Something went wrong. " + ex.Message.ToString());
            }


        }
    }
}
